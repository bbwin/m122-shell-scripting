#!/usr/bin/env bats

@test "prime.sh returns 0 for a prime number" {
    run ./prime.sh 13
    [ "$status" -eq 0 ]
}

@test "prime.sh returns 1 for a non-prime number" {
    run ./prime.sh 8
    [ "$status" -eq 1 ]
}

@test "prime.sh returns 2 for a non-number" {
    run ./prime.sh not_a_number
    [ "$status" -eq 2 ]
}

@test "prime.sh prints help text when run without an argument" {
    run ./prime.sh
    [ "$status" -eq 1 ]
    [ "$output" = "Usage: ./prime.sh [-v] number" ]
}

@test "is_number() returns 0 for a number" {
    source ./prime.sh
    run is_number 47
    [ "$status" -eq 0 ]
}

@test "is_number() returns 1 for a non-number" {
    source ./prime.sh
    run is_number not_a_number
    [ "$status" -eq 1 ]
}

@test "is_prime() returns 0 for a prime number" {
    source ./prime.sh
    run is_prime 277
    [ "$status" -eq 0 ]
    run is_prime 2221
    [ "$status" -eq 0 ]
    run is_prime 7103
    [ "$status" -eq 0 ]
}

@test "is_prime() returns 1 for a non-prime number" {
    source ./prime.sh
    run is_prime 275
    [ "$status" -eq 1 ]
    run is_prime 2223
    [ "$status" -eq 1 ]
    run is_prime 7101
    [ "$status" -eq 1 ]
}

@test "ShellCheck should pass without any errors" {
    run shellcheck ./prime.sh
    [ "$status" -eq 0 ]
}