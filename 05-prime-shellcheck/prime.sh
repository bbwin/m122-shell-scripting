#!/bin/bash

set -euo pipefail
verbose=0

debug() {
    # Print debug messages if verbose mode is enabled
    if [ $verbose -eq 1 ]; then
        echo -e "$1"
    fi
}

function is_number {
    # Check for numbers
    # Arguments:
    #   $1: number
    # Return Codes:
    #   0: is number
    #   1: not a number

    local number=$1

    if [ "$number" -eq "$number" ] 2>/dev/null; then
        return 0
    else
        return 1
    fi
}

function is_prime {
    # Check for prime numbers
    # Arguments: $1 -> number
    # Return Codes:
    # 0 -> is prime
    # 1 -> not prime

    local number=$1
    
    # Special case: Numbers lower or equal 1 are not prime
    if [ "$number" -le 1 ]; then
        return 1
    fi

    # Numbers 2 and 3 are prime
    if [ "$number" -eq 2 ] || [ "$number" -eq 3 ]; then
        return 0
    fi

    # Numbers divisible by 2 or 3 are not prime
    debug "Testing devisor: 2, 3"
    if [ $((number % 2)) -eq 0 ] || [ $((number % 3)) -eq 0 ]; then
        return 1
    fi

    # Check divisor from 5 up to sqare root of number
    divisor=5
    while [ $((divisor * divisor)) -lt "$number" ]; do
        debug "Testing devisor: $divisor, $((divisor + 2))"
        if [ $((number % divisor)) -eq 0 ] || [ $((number % divisor + 2)) -eq 0 ]; then
            return 1
        fi
        divisor=$((divisor + 6))
    done

    return 0
}

main() {
    if [ "$#" -eq 0 ]; then
        echo "Usage: $0 [-v] number"
        exit 1
    fi

    for arg in "$@"; do
        if [ "$arg" == "-v" ]; then
            verbose=1
        else
            number="$arg"
        fi
    done

    if ! is_number $number; then
        echo "$number is not a number." >&2
        exit 2
    fi

    if is_prime $number; then
        echo -e "$number is PRIME."
    else
        echo -e "$number is NOT prime."
        exit 1
    fi
}

# Check if the script is being sourced or executed
if [[ "${BASH_SOURCE[0]:-}" == "${0}" ]]; then
    main "$@"
fi