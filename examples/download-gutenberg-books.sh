#!/bin/bash

# Download Gutenberg Books
mkdir -p gutenberg
wget https://www.gutenberg.org/ebooks/84.txt.utf-8 -O "gutenberg/Frankenstein.txt"
wget https://www.gutenberg.org/ebooks/1342.txt.utf-8 -O "gutenberg/Pride and Prejudice.txt"
wget https://www.gutenberg.org/ebooks/1513.txt.utf-8 -O "gutenberg/Romeo and Juliet.txt"
wget https://www.gutenberg.org/ebooks/2701.txt.utf-8 -O "gutenberg/Moby Dick.txt"
wget https://www.gutenberg.org/ebooks/2641.txt.utf-8 -O "gutenberg/A Room with a View.txt"
wget https://www.gutenberg.org/ebooks/37106.txt.utf-8 -O "gutenberg/Little Women.txt"
wget https://www.gutenberg.org/ebooks/11.txt.utf-8 -O "gutenberg/Alice's Adventures in Wonderland.txt"  