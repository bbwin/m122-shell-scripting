#!/bin/bash

# Bash doesn't know bolean
USERARG=0

# [ "$1" ] is the same as ! [ -z "$1" ]
if [ "$1" ]; then
    USERARG=1
    echo "I received input: $1"
fi

if [ $USERARG -ne 1 ]; then
    echo "Nothing to do here"
    exit 0
fi

if [ "$1" == "status" ]; then
    echo "Happy"
elif [ "$1" == "weather" ]; then
    echo "No clue"
elif [ "$1" == "date" ]; then
    date
elif [ -f "$1"  ]; then
    if ! grep "robot" "$1"; then
        echo "No robots" >&2
        exit 1
    fi
else
    echo "Unknown command: $1" >&2
    exit 1
fi